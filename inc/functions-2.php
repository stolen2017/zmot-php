<?php
function get_dt_targets(){
    include("connection.php");
    
    try {
        //$sql = "SELECT * FROM targets ORDER BY 'target_description' DESC";
		$sql = "SELECT * FROM target_names ORDER BY target_description DESC";
        $result = $db->prepare($sql);
        $result->execute();
        
    } catch (Exception $e) {
      echo "bad query";
    }
    
    //$dtTagets = $result->fetchAll(PDO::FETCH_ASSOC);
    $dtTagets = $result->fetchAll(PDO::FETCH_ASSOC);
	return $dtTagets;
}

function get_utm_parameters(){
    include("connection.php");
    
    try {
		$sql = "SELECT * FROM `utm_params` ORDER BY campaign";
        $result = $db->prepare($sql);
        $result->execute();
        
    } catch (Exception $e) {
      echo "bad query";
    }
    
    //$dtTagets = $result->fetchAll(PDO::FETCH_ASSOC);
    $utmTagets = $result->fetchAll(PDO::FETCH_ASSOC);
	return $utmTagets;
}

function get_cardinale_cs(){
    include("connection.php");

    try{
        $sql = "SELECT * FROM cardinale_cs ORDER BY dealer";
        $result = $db->prepare($sql);
        $result->execute();

    } catch (Exception $e){
        echo "bad query";
    }

    $csTagets = $result->fetchAll(PDO::FETCH_ASSOC);
	return $csTagets;
}
    
    
function get_dt_html($item) {
    //$output = "<tr><td>". $item["?Target Name"] ."  </td><td>
    //         ". $item["Target Description"] . "</td></tr>";
	//$output = "<tr><td>". $item["?target_name"] ."  </td><td>
              //". $item["target_description"] . "</td></tr>";
	$output = "<tr><td>". $item["?target"] ."  </td><td>
              ". $item["target_description"] . "</td></tr>";
    return $output;
}

function get_utm_html($item) {
    //$output = "<tr><td>". $item["Target Name"] ."  </td><td>
	$output = "<tr><td>". $item["target_name"] ."  </td><td>
              ". $item["referrer"] . "</td><td>
              ". $item["content"] . "</td><td>
              ". $item["campaign"] . "</td><td>
              ". $item["term"] . "</td><td>
              ". $item["medium"] . "</td></tr>";
    return $output;
}

function get_cs_html($item){
    $output = "<tr><td>". $item["dealer"] ."  </td><td>
              ". $item["callsource_type"] . "</td><td>
              ". $item["number"] . "</td><td>
              ". $item["crm"] . "</td></tr>";
    return $output;
}