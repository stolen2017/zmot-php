<?php 

include("inc/functions-2.php");

$pageTitle = "ZMOT Automotive";
$section = null;

include("inc/header.php"); ?>
		<div class="section catalog random">

			<div class="wrapper">

				<h2>UTM Parameters</h2>

        		<table class="Targets">
        			<tr>
        				<th>Target Name</th>
        				<th>Referrer</th>
        				<th>Content</th>
        				<th>Campaign</th>
        				<th>Term</th>
        				<th>Medium</th>
        			</tr>
            <?php

			$random = get_utm_parameters();
			
            foreach ($random as $item){
            	echo get_utm_html($item);
            }
            
            ?>							
				</table>

			</div>

		</div>

<?php include("inc/footer.php"); ?>